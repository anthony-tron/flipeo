import browser from 'webextension-polyfill';

let isFlipped = false;

browser.runtime.onMessage.addListener((message) => {
  isFlipped = !isFlipped;

  if (message?.type !== 'flipVideo') {
    return;
  }

  const element = document.querySelector('#movie_player video');

  if (element?.parentElement) {
    element.parentElement.style.transform = `scaleX(${isFlipped ? '-1' : '1'})`;
  }
});
