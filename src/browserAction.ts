import browser from 'webextension-polyfill';

browser.action.onClicked.addListener((currentTab) => {
  if (!currentTab?.id) {
    return;
  }

  browser.tabs.sendMessage(currentTab.id, {
    type: 'flipVideo',
  });
});
