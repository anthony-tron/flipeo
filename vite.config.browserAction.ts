import { defineConfig } from 'vite';
import { resolve } from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  publicDir: false,
  build: {
    emptyOutDir: false,
    outDir: resolve(__dirname, 'build'),
    lib: {
      formats: ['iife'],
      entry: resolve(__dirname, 'src', 'browserAction.ts'),
      name: 'Flipeo',
    },
    rollupOptions: {
      output: {
        entryFileNames: 'browserAction.js',
        extend: true,
        sourcemap: true,
      },
    },
  },
});
