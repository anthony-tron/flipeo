# Flipeo

Flipeo is a cross-platform browser extension. Use it to flip videos horizontally on YouTube.


## Developping

Run the development servers in two separate shells:

1. `npm run dev` (restart if changing `.env.local` or `manifest.json`)
2. `npm run dev:browserAction`

This will start dev mode for Firefox.

To develop on Chrome, create a `.env.local` file:
```
VITE_TARGET=chrome
```

> HMR is supported for files in `src/`, but not for files in `public/`.

