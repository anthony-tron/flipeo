import { defineConfig, loadEnv } from 'vite';
import { resolve } from 'path';
import { viteStaticCopy } from 'vite-plugin-static-copy';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd());

  const target = env.VITE_TARGET || 'firefox';

  return {
    build: {
      emptyOutDir: false,
      outDir: resolve(__dirname, 'build'),
      lib: {
        formats: ['iife'],
        entry: resolve(__dirname, 'src', 'contentScript.ts'),
        name: 'Flipeo',
      },
      rollupOptions: {
        output: {
          entryFileNames: 'contentScript.js',
          extend: true,
          sourcemap: true,
        },
      },
    },

    plugins: [
      viteStaticCopy({
        targets: [
          {
            src: `src/manifest.${target}.json`,
            dest: '.',
            rename: 'manifest.json',
          },
        ],
      }),
    ],
  };
});
